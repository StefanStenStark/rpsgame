package com.example.rpsgame;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class GameService {
    private final GameRepository gameRepository;
    private final PlayerService playerService;


    @Autowired
    public GameService(GameRepository gameRepository, PlayerRepository playerRepository, PlayerService playerService) {
        this.gameRepository = gameRepository;
        this.playerService = playerService;
    }


    public UUID getToken( ){
        UUID token = UUID.randomUUID();
        return token;
    }

    public Game startGame(String playerOneId) {
        String gameId = UUID.randomUUID().toString();
        Game newGame  = new Game(
                gameId,
                "OPEN",
                playerOneId,
                null,
                null,
                null,
                null,
                null
        );

        gameRepository.save(newGame);
        return newGame;


    }

    public Optional<Game> joinGame(String playerTwoId, String gameId) {
        return gameRepository.findById(gameId)
                .map(game -> {
                    game.setPlayerTwoId(playerTwoId);
                    game.setGameStatus("ACTIVE");

                    gameRepository.save(game);
                    return game;
                });

    }
    public List<Game> getGames() {
        return gameRepository.findAll();
    }

    public Optional<Game> gameInfo(String playerId, String gameId) {
        return gameRepository.findById(gameId)
                .map(game -> {
                        if (game.getPlayerOneMove().equals("ROCK")){
                            if (game.getPlayerTwoMove().equals("ROCK")){
                                game.setPlayerOneWin("DRAW");
                                game.setPlayerTwoWin("DRAW");
                            }
                            if (game.getPlayerTwoMove().equals("SCISSOR")){
                                game.setPlayerOneWin("WIN");
                                game.setPlayerTwoWin("LOSE");
                            }
                            if (game.getPlayerTwoMove().equals("PAPER")){
                                game.setPlayerOneWin("LOSE");
                                game.setPlayerTwoWin("WIN");
                            }
                        }
                        if (game.getPlayerOneMove().equals("SCISSOR")){
                            if (game.getPlayerTwoMove().equals("ROCK")){
                                game.setPlayerOneWin("LOSE");
                                game.setPlayerTwoWin("WIN");
                            }
                            if (game.getPlayerTwoMove().equals("SCISSOR")){
                                game.setPlayerOneWin("DRAW");
                                game.setPlayerTwoWin("DRAW");
                            }
                            if (game.getPlayerTwoMove().equals("PAPER")){
                                game.setPlayerOneWin("WIN");
                                game.setPlayerTwoWin("LOSE");
                            }
                        }
                        if (game.getPlayerOneMove().equals("PAPER")){
                            if (game.getPlayerTwoMove().equals("ROCK")){
                                game.setPlayerOneWin("WIN");
                                game.setPlayerTwoWin("LOSE");
                            }
                            if (game.getPlayerTwoMove().equals("SCISSOR")){
                                game.setPlayerOneWin("LOSE");
                                game.setPlayerTwoWin("WIN");
                            }
                            if (game.getPlayerTwoMove().equals("PAPER")){
                                game.setPlayerOneWin("DRAW");
                                game.setPlayerTwoWin("DRAW");
                            }
                        }
                    gameRepository.save(game);
                    return game;
                });

    }


    public Optional<Game> makeMove(String playerId, String gameId, String sign) {

        return gameRepository.findById(gameId)
                .map(game -> {
                    if (playerId.equals(game.getPlayerOneId())){
                        game.setPlayerOneMove(sign);
                    }
                    if (playerId.equals(game.getPlayerTwoId())){
                        game.setPlayerTwoMove(sign);
                    }
                    gameRepository.save(game);
                    return game;
                });
    }
}
