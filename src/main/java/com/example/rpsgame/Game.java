package com.example.rpsgame;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Entity(name = "Game")
@Table(name = "Game")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Game {

    @Id
    @Column(name = "Id")
    String id;
    @Column(name = "GameStatus")
    String gameStatus;
    @Column(name = "PlayerOneId")
    String playerOneId;
    @Column(name = "PlayerTwoId")
    String playerTwoId;
    @Column(name = "PlayerOneMove")
    String PlayerOneMove;
    @Column(name = "PlayerTwoMove")
    String PlayerTwoMove;
    @Column(name = "PlayerOneWin")
    String PlayerOneWin;
    @Column(name = "PlayerTwoWin")
    String PlayerTwoWin;
}

