package com.example.rpsgame;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "api/rps/games")
public class GameController {


    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }


    @GetMapping("/auth/token")
    public UUID getToken(){
        return gameService.getToken();
    }

    @PostMapping("/games/start")
    public void startGame(@RequestHeader("id") String id) {
        gameService.startGame(
                id);
    }

    @GetMapping("/games/join/{gameId}")
    public void joinGame(@RequestHeader("id") String playerTwoId,@PathVariable("gameId")String gameId) {
        gameService.joinGame(
                playerTwoId,
                gameId);
    }
    @GetMapping
    public List<Game> getGames(){
        return gameService.getGames();
    }
    @GetMapping("/games/{Id}")
    public void gameInfo(@RequestHeader("id") String playerId,@PathVariable("Id")String gameId) {
        gameService.gameInfo(
                playerId,
                gameId);
    }


    @PostMapping("/games/move/{sign}")
    public void makeMove(@RequestHeader("id") String playerId,@RequestHeader("gameid") String gameId,@PathVariable("sign")String sign) {
        gameService.makeMove(
                playerId,
                gameId,
                sign);
    }



}
