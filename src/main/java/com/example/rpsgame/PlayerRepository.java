package com.example.rpsgame;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public interface PlayerRepository extends JpaRepository<Player, String> {

}
