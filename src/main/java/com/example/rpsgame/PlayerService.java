package com.example.rpsgame;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class PlayerService {
    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public UUID getToken( ){
        UUID token = UUID.randomUUID();
        return token;
    }


    public Player createPlayer(String id,String name) {
        Player newPlayer = new Player(
                id,
                name,
                null
        );
        playerRepository.save(newPlayer);
        return newPlayer;
    }

}
