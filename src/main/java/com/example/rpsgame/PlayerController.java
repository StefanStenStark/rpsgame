package com.example.rpsgame;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "api/rps")
public class PlayerController {


    private final PlayerService playerService;

    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping("/auth/token")
    public UUID getToken(){
        return playerService.getToken();
    }

    @PostMapping("/user/name")
    public void createPlayer(@RequestHeader("id") String id,@RequestBody Player player) {
        playerService.createPlayer(
                id,
                player.getName());
    }



}
