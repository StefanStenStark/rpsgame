package com.example.rpsgame;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.List;
import java.util.UUID;

import static jakarta.persistence.GenerationType.SEQUENCE;

@Entity(name = "player")
@Table(name = "player")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Player {


    @Id
    @Column(name = "id")
    String Id;
    @Column(name = "name")
    String name;
    @Column(name = "game")
    String game;

}